install:
	pip install -r requirements.txt
test:
	python -m unittest -v test.py
download:
	chmod +x ./download_embeddings.sh
	./download_embeddings.sh
df:
	python create_data_frames.py
explore:
	python explore_relations.py
features:
	python extract_features.py
small_features:
	python extract_small_features.py ave_entire
	python extract_small_features.py ave_padded
	python extract_small_features.py stacked
feedforward:
	python run_ff.py
convolutional:
	python run_cnn.py
