# Processes sentences into data frames and creates
# additional column where Arg1 Connective and Arg2
# have been concatenated into one long string.

import os
import pickle
import string
import pandas as pd
import preprocessing as pp

# Important constants
PUNC = set(string.punctuation)
OUTPUT_PATH = './processed_sentences'
OUTPUT_FORMAT = 'pkl' # 'pickle'

# helper functions
concatenation = lambda row: f"{row.Arg1}, {row.Connective} {row.Arg2}"
rm_punc = lambda s: "".join(c for c in s if c not in PUNC)

# create folder if it doesnt exist
if not os.path.exists(OUTPUT_PATH):
    print(f'"{OUTPUT_PATH}" not found. Creating folder...')
    os.system(f'mkdir {OUTPUT_PATH}')

# load data
print('Loading original data with preprocessing.py ....')
data = pp.load_data(text_only=True)
train = data['train']
dev = data['dev']
test = data['test']

# convert to data frames
print('Converting to data frames ...')
df_train = pd.DataFrame(train)
df_dev = pd.DataFrame(dev)
df_test = pd.DataFrame(test)
dfs = [df_train, df_dev, df_test]

# perform concat
print('Performing Arg1, Connective, Arg2 concatenation ...')
for df in dfs:
    col = df.apply(concatenation, axis=1)
    df['Concatenated'] = col

print('Removing punctuation')
train_sense = df_train.Sense.tolist()
df_train = df_train.applymap(rm_punc)
df_train['Sense'] = train_sense

dev_sense = df_dev.Sense.tolist()
df_dev = df_dev.applymap(rm_punc)
df_dev['Sense'] = dev_sense

test_sense = df_test.Sense.tolist()
df_test = df_test.applymap(rm_punc)
df_test['Sense'] = test_sense

dfs = [df_train, df_dev, df_test]

fnames = [
    f'{fn}.{OUTPUT_FORMAT}' 
    for fn in ['train', 'dev', 'test']
]

print(f'Dumping using output format {OUTPUT_FORMAT}')
for fname, df in zip(fnames, dfs):
    p = f"{OUTPUT_PATH}/{fname}"
    if OUTPUT_FORMAT == 'csv':
        df.to_csv(p, index=False)
    elif OUTPUT_FORMAT in ['pickle', 'pkl']:
        with open(p, 'wb') as f:
            pickle.dump(df, f)
