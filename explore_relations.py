import pandas as pd
import numpy as np

print('Let\'s explore some training data!')

INPUT_PATH = './processed_sentences/train.pkl'
HEAD_SIZE = 30

# load raw training data
train = pd.read_pickle(INPUT_PATH)
arg1 = train['Arg1']
arg2 = train['Arg2']
conn = train['Connective']

for column, name in zip((arg1, arg2, conn), ('Arg1', 'Arg2', 'Connective')):
    print(f'Summary for number of tokens in {name}')
    summary = column.apply(lambda s: len(s.split()))\
                    .value_counts(normalize=True)\
                    .apply(lambda f: f"{min(40, round(100*f))*'#'} ({round(100*f)}%)")\
                    .sort_index()\
                    .head(HEAD_SIZE)#describe()
    print(summary)
    if name == 'Connective':
        print('Most common connectives')
        print(column.apply(lambda s: s if s else "<empty>")\
                    .value_counts(normalize=True)\
                    .apply(lambda f: f"{min(40, round(100*f))*'#'} ({round(100*f)}%)")\
                    .head(HEAD_SIZE))
    print()

