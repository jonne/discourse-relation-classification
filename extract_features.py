import word_embeddings as we
import sklearn.preprocessing as pp
import itertools as it
import pandas as pd
import helpers as h
import numpy as np
import json
import os

# constants
BASE_PATH = './processed_sentences'
FEATURES_PATH = './features'
HYPERPARAMETERS_PATH = './hyperparameters'
LAYER_SIZES = [64, 128, 256]
N_ARG_TOKENS_SIZE = 25
N_CONN_TOKENS = 2

# check if FEATURES_PATH and HYPERPARAMETERS_PATH exist and if not create them
for p in (FEATURES_PATH, HYPERPARAMETERS_PATH):
    if not os.path.exists(p):
        print(f'No folder found under "{p}". Creating it...')
        os.system(f'mkdir {p}')
    else:
        print(f'Folder "{p}" successfully found...')

FUNNEL_SIZES = [[s] for s in LAYER_SIZES] + \
                [[a,b] for a,b in it.permutations(LAYER_SIZES, 2) if a > b] + \
                [[256, 128, 64]]
SL_SIZES = [[s,l] for s,l in it.permutations(LAYER_SIZES, 2) if s < l]

LAYER_CONDITIONS = {
    'funnel': [[512] + f for f in FUNNEL_SIZES],
    'bottleneck': [[512] + sl for sl in SL_SIZES],
    'double_bottleneck': [[512] + sl + sl for sl in SL_SIZES]
}

FLAT_LAYER_CONDITIONS = list(it.chain.from_iterable(LAYER_CONDITIONS.values()))

print(f'Saving layer configurations to {HYPERPARAMETERS_PATH}...')
h.dump_pickle(LAYER_CONDITIONS, f'{HYPERPARAMETERS_PATH}/layer_conditions.pkl')
h.dump_pickle(FLAT_LAYER_CONDITIONS, f'{HYPERPARAMETERS_PATH}/flat_layer_conditions.pkl')

# define function for one-hot encoding a series
def one_hot_encoder(series):
    ohe = pp.OneHotEncoder()
    ohe.fit(series.to_numpy().reshape(-1, 1))
    return ohe

def one_hot_encode(ohe, series):
    out =  ohe.transform(series.to_numpy()
                               .reshape(-1, 1))\
              .todense()
    out = np.array(out)
    return out

# create embedders
print('Loading embeddings...')
fast_text_emb = we.FastTextEmbedding()
stacked_embedding = we.StackedEmbedding(fast_text_emb)
average_embedding = we.AverageEmbedding(fast_text_emb)

# load train dev and test data
print('Loading processed data...')
train = pd.read_pickle(f"{BASE_PATH}/train.pkl")
dev = pd.read_pickle(f"{BASE_PATH}/dev.pkl")
test = pd.read_pickle(f"{BASE_PATH}/test.pkl")

# instantiate and train one hot encoder
print('Encoding labels with 1-hot encoding...')
ohe = one_hot_encoder(train.Sense)
train_labels = one_hot_encode(ohe, train.Sense)
dev_labels = one_hot_encode(ohe, dev.Sense)
test_labels = one_hot_encode(ohe, test.Sense)

labels = {
    'train': train_labels,
    'dev': dev_labels,
    'test': test_labels
}

print('Done! Pickling...')
h.dump_pickle(labels, f"{FEATURES_PATH}/one_hot_labels.pkl")
h.dump_pickle(ohe, f"{FEATURES_PATH}/one_hot_encoder.pkl")

# grab paddded/truncated tokens from arg1, arg2, and conn
print('Done! Now extracting paddeed/truncated tokens...')
grab_arg1 = lambda s: h.grab_tokens(s, N_ARG_TOKENS_SIZE, from_end=True)
grab_arg2 = lambda s: h.grab_tokens(s, N_ARG_TOKENS_SIZE, from_end=False)
grab_conn = lambda s: h.grab_tokens(s, N_CONN_TOKENS, from_end=False)

extracted_tokens = {}
for name, df in (('train', train), ('dev', dev), ('test', test)):
    print(f"This is '{name}'")
    arg1_tokens = df.Arg1.apply(grab_arg1)
    arg2_tokens = df.Arg2.apply(grab_arg2)
    conn_tokens = df.Connective.apply(grab_conn)
    # "joined" is a list of lists
    joined = [
        arg1 + conn + arg2 
        for arg1, conn, arg2 
        in zip(arg1_tokens, 
               arg2_tokens, 
               conn_tokens)
    ]
    extracted_tokens[name] = joined

print('Done! Pickling...')
h.dump_pickle(extracted_tokens, f"{FEATURES_PATH}/extracted_tokens.pkl")

# stacked & averaged sentence vectors based on padded/truncated
print('Done. Now calculating embeddings based on the truncated/padded tokens...')
stacked_embeddings_padded = {}
average_embeddings_padded = {}
for name, tokens in extracted_tokens.items():
    print(f"This is '{name}'")
    embs_stacked = np.stack([stacked_embedding.transform(tokens=ts) for ts in tokens], axis=0)
    embs_averaged = np.stack([average_embedding.transform(tokens=ts) for ts in tokens], axis=0)
    stacked_embeddings_padded[name] = embs_stacked
    average_embeddings_padded[name] = embs_averaged

print('Done! Pickling...')
h.dump_pickle(
    stacked_embeddings_padded, 
    f'{FEATURES_PATH}/stacked_embeddings_padded.pkl'
)


h.dump_pickle(
    average_embeddings_padded, 
    f'{FEATURES_PATH}/average_embeddings_padded.pkl'
)

# average sentence vector based on ENTIRE concatenation of Arg1+Conn+Arg2
print('Done! Calculating embeddings based on ENTIRE concatenation of Arg1+Conn+Arg2')
average_embeddings_entire = {}

for name, df in (('train', train), ('dev', dev), ('test', test)):
    print(f'This is "{name}"')
    tokens = df.Concatenated.apply(lambda s: s.split()).tolist()
    embs = np.stack([average_embedding.transform(tokens=ts) for ts in tokens], axis=0)
    average_embeddings_entire[name] = embs

print('Done! Pickling...')
h.dump_pickle(
    average_embeddings_entire, 
    f'{FEATURES_PATH}/average_embeddings_entire.pkl'
)

print('All done!')
