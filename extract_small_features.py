import helpers as h
import numpy as np
import sys
import os

if len(sys.argv) < 2:
    raise ValueError('Usage: python extract_small_features.py <feature type>')

# constants
FEATURES_PATH = './features'
SMALL_FEATURES_PATH = './features/small'
SMALL_SAMPLE_SIZE = 500

FEATURE_FILE_NAMES = {
    "ave_entire": "average_embeddings_entire.pkl",
    "ave_padded": "average_embeddings_padded.pkl",
    "stacked": "stacked_embeddings_padded.pkl"
}

feature_type = sys.argv[1]
if feature_type not in FEATURE_FILE_NAMES:
    raise ValueError('Feature type must be one of "{}", "{}", "{}"'.format(*FEATURE_FILE_NAMES.keys()))

# create folder if doesn't exist
if not os.path.exists(SMALL_FEATURES_PATH):
    os.system(f'mkdir -p {SMALL_FEATURES_PATH}')

# get labels
labels = h.load_pickle(f'{FEATURES_PATH}/one_hot_labels.pkl')
train_labels = labels['train']
dev_labels = labels['dev']
test_labels = labels['test']

# get features
feature_file = FEATURE_FILE_NAMES[feature_type]
features = h.load_pickle(f'{FEATURES_PATH}/{feature_file}')
train_features = features['train']
dev_features = features['dev']
test_features = features['test']

small_labels = {}
small_features = {}
for kind in ('train', 'dev', 'test'):
    print(f'Extracting "{kind}" features of type "{feature_type}"')
    small_labels[kind] = labels[kind][:SMALL_SAMPLE_SIZE]
    small_features[kind] = features[kind][:SMALL_SAMPLE_SIZE]

print('Done! Pickling...')
h.dump_pickle(small_labels, f'{SMALL_FEATURES_PATH}/one_hot_labels.pkl')
h.dump_pickle(small_features, f'{SMALL_FEATURES_PATH}/{feature_file}')
print('All done!')
