import word_embeddings as we
import preprocessing as pp
import experiment as e
import pandas as pd
import unittest


# load embeddings
fast_text_emb = we.FastTextEmbedding()
stacked_sent_emb = we.StackedEmbedding(fast_text_emb)
averaged_sent_emb = we.AverageEmbedding(fast_text_emb)

# load data
train = pd.read_pickle("processed_sentences/train.pkl")
dev = pd.read_pickle("processed_sentences/dev.pkl")
test = pd.read_pickle("processed_sentences/test.pkl")
original_relations = pp.load_data(verbose=True, text_only=False)

# tests

TEST_SENTENCE = 'i love nlp'

class TestProjectTwo(unittest.TestCase):
    def test_fast_text_dim_is_300(self):
       v = fast_text_emb.transform(TEST_SENTENCE)
       self.assertEqual(v.shape, (300,))

    def test_stacked_sent_emb_is_3_by_300(self):
        v = stacked_sent_emb.transform(TEST_SENTENCE)
        self.assertEqual(v.shape, (3, 300))

    def test_averaged_sent_emb_is_300(self):
        v = averaged_sent_emb.transform(TEST_SENTENCE)
        self.assertEqual(v.shape, (300,))

    def test_train_contains_all_senses(self):
        train_senses = set(train.Sense.unique().tolist())
        dev_senses = set(dev.Sense.unique().tolist())
        test_senses = set(test.Sense.unique().tolist())
        self.assertEqual(dev_senses-train_senses, set())
        self.assertEqual(test_senses-train_senses, set())

    def test_run_stacked_padded_ff_exp(self):
        _ = e.StackedPaddedFeedforwardExperiment(
                layer_sizes=[512, 256, 64],
                small=True, n_epochs=3,
                original_relations=original_relations)
        _.run()
        _.save()

    def test_run_average_padded_ff_exp(self):
        _ = e.AveragePaddedFeedforwardExperiment(
                layer_sizes=[512, 256, 64], 
                small=True, n_epochs=3,
                original_relations=original_relations)
        _.run()
        _.save()

    def test_init_average_entire_ff_exp(self):
        _ = e.AverageEntireFeedforwardExperiment(
                layer_sizes=[512, 256, 64],
                small=True, n_epochs=3,
                original_relations=original_relations)
        _.run()
        _.save()

if __name__ == '__main__':
    unittest.main()


